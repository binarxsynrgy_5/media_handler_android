package com.rahmanarifofficial.mediahandleaplication

import android.Manifest
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.bumptech.glide.Glide
import com.rahmanarifofficial.mediahandleaplication.databinding.ActivityHandleImageBinding
import java.io.File

class HandleImageActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHandleImageBinding
    private lateinit var uri: Uri

    private val cameraResult =
        registerForActivityResult(ActivityResultContracts.TakePicture()) { result ->
            if (result) {
                handleImage(uri)
            }
        }
    private val galeryResult =
        registerForActivityResult(ActivityResultContracts.GetContent()) { result ->
            if (result!=null){
                handleImage(result)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityHandleImageBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupEventUI()
    }

    private fun setupEventUI() {
        binding.btnChooseCamera.setOnClickListener { openCamera() }
        binding.btnChooseGallery.setOnClickListener { openGallery() }
    }

    private fun openCamera() {
        if (!PermissionUtils.isPermissionGranted(this, Manifest.permission.CAMERA)) {
            PermissionUtils.requestPermission(
                this,
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                PermissionUtils.REQUEST_CODE_CAMERA
            )
        } else {
            //JALANKAN LOGIC
            val photoFile = File.createTempFile(
                "IMG_",
                ".jpg",
                this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            )

            uri = FileProvider.getUriForFile(
                this,
                "${this.packageName}.provider",
                photoFile
            )
            cameraResult.launch(uri)
        }
    }

    private fun openGallery() {
        if (!PermissionUtils.isPermissionGranted(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        ) {
            PermissionUtils.requestPermission(
                this,
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                PermissionUtils.REQUEST_CODE_GALERY
            )
        } else {
            intent.type = "image/*"
            galeryResult.launch("image/*")
        }
    }

    private fun handleImage(uri: Uri) {
        Glide.with(this)
            .load(uri)
            .into(binding.ivImage)

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PermissionUtils.REQUEST_CODE_CAMERA) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(
                    this@HandleImageActivity,
                    "Camera Permission Granted",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                Toast.makeText(
                    this@HandleImageActivity,
                    "Camera Permission Denied",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        if (requestCode == PermissionUtils.REQUEST_CODE_GALERY) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(
                    this@HandleImageActivity,
                    "Permission Granted",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                Toast.makeText(
                    this@HandleImageActivity,
                    "Permission Denied",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

}