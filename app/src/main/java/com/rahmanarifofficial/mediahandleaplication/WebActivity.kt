package com.rahmanarifofficial.mediahandleaplication

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebViewClient
import com.rahmanarifofficial.mediahandleaplication.databinding.ActivityWebBinding

class WebActivity : AppCompatActivity() {

    private lateinit var binding: ActivityWebBinding
    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityWebBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val url = "https://docs.google.com/gview?embedded=true&url=${getPdfUrl()}"
        binding.webView.settings.supportZoom()
        binding.webView.settings.domStorageEnabled = true
        binding.webView.settings.javaScriptEnabled = true
        binding.webView.loadUrl(url)
    }

    fun getPdfUrl(): String =
        "https://kotlinlang.org/assets/kotlin-media-kit.pdf"
}