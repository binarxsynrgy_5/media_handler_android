package com.rahmanarifofficial.mediahandleaplication

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.rahmanarifofficial.mediahandleaplication.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupEventUI()
    }

    private fun setupEventUI() {
        binding.btnPdfHandle.setOnClickListener {
            startActivity(Intent(this, HandlePDFActivity::class.java))
        }
        binding.btnImageHandle.setOnClickListener {
            startActivity(Intent(this, HandleImageActivity::class.java))
        }
        binding.btnVideoHandle.setOnClickListener {
            startActivity(Intent(this, VideoViewActivity::class.java))
        }
    }
}