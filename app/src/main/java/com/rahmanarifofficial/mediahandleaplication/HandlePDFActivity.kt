package com.rahmanarifofficial.mediahandleaplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.rahmanarifofficial.mediahandleaplication.databinding.ActivityHandlePdfactivityBinding

class HandlePDFActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHandlePdfactivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityHandlePdfactivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupEventUI()
    }

    private fun setupEventUI(){
        binding.buttonWebView.setOnClickListener {
            startActivity(Intent(this, WebActivity::class.java))
        }
        binding.buttonAssets.setOnClickListener {
            startActivity(Intent(this, PDFViewActivity::class.java).putExtra(PDFViewActivity.FLAG, PDFViewActivity.ASSETS))
        }
        binding.buttonStorage.setOnClickListener {
            startActivity(Intent(this, PDFViewActivity::class.java).putExtra(PDFViewActivity.FLAG, PDFViewActivity.STORAGE))
        }
        binding.buttonInternet.setOnClickListener {
            startActivity(Intent(this, PDFViewActivity::class.java).putExtra(PDFViewActivity.FLAG, PDFViewActivity.INTERNET))
        }
    }
}