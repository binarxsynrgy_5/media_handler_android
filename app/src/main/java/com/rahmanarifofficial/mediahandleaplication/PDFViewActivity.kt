package com.rahmanarifofficial.mediahandleaplication

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.registerForActivityResult
import com.pspdfkit.configuration.activity.PdfActivityConfiguration
import com.pspdfkit.ui.PdfActivity
import com.rahmanarifofficial.mediahandleaplication.databinding.ActivityPdfviewBinding

/*
* Documentation PDFActivity
* https://pspdfkit.com/blog/2022/how-to-build-an-android-pdf-viewer/
* */

class PDFViewActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPdfviewBinding
    private val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                //  you will get result here in result.data
                val intent = result.data
                val uri = intent?.data
                val config = PdfActivityConfiguration.Builder(applicationContext).build()
                uri?.let {
                    PdfActivity.showDocument(this, uri, config)
                }
            }
        }


    companion object {
        const val FLAG = "FLAG"
        const val ASSETS = "ASSETS"
        const val STORAGE = "STORAGE"
        const val INTERNET = "INTERNET"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityPdfviewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        /*
        * Jika parameter yang dikirim itu = assets maka dia akan X
        * Jika parameter yang dikirim itu = storage maka dia akan Y
        * Jika parameter yang dikirim itu = internet maka dia akan Z
        * */

        val params = intent.getStringExtra(FLAG)
        if (params.equals(ASSETS)) {
            val uri = Uri.parse("file:///android_asset/${getPdfNameFromAssets()}")
            val config = PdfActivityConfiguration.Builder(applicationContext).build()
            PdfActivity.showDocument(this, uri, config)
        } else if (params.equals(STORAGE)) {
            selectPdfFromStorage()
        } else {

        }
    }

    private fun getPdfNameFromAssets(): String = "sample.pdf"

    private fun selectPdfFromStorage() {
        val browseStorage = Intent(Intent.ACTION_GET_CONTENT)
        browseStorage.type = "application/pdf"
        browseStorage.addCategory(Intent.CATEGORY_OPENABLE)
        startForResult.launch(Intent.createChooser(browseStorage, "Select PDF"))
    }


}